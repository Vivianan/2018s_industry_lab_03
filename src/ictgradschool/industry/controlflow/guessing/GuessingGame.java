package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {
    int goal;
    int guess;

    public void start() {

        // TODO Write your code here.
        goal=(int)(Math.random()*100)+1;
        guess=0;
        while(guess!=goal){
            System.out.printf("Enter your guess (1-100):");
            guess=Integer.parseInt(Keyboard.readInput());
            if(guess>goal){
                System.out.println("Too high, try again");
            }
            else if(guess<goal){
                System.out.println("Too low, try again");
            }
            else{
                System.out.println("Perfect!");
            }

        }
        System.out.println("Goodbye");
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
